#!/usr/bin/env python
from subprocess import call
import pathlib
import os

HERE = pathlib.Path(__file__).parent
version = (HERE / "VERSION").read_text().strip()

def get_new_setup_py_lines(this_file):
    with open(this_file, 'r') as sf:
        current_setup = sf.readlines()
    for line in current_setup:
        if line.startswith('VERSION = '):
            yield "VERSION = '{}'\n".format(version)
        else:
            yield line

lines = list(get_new_setup_py_lines('setup.cfg'))
with open('setup.cfg', 'w') as sf:
    sf.writelines(lines)


lines = list(get_new_setup_py_lines('setup.py'))
with open('setup.py', 'w') as sf:
    sf.writelines(lines)


env = os.environ
call('rm -rf dist/*', shell=True, env=env)
call('python -m build', shell=True, env=env)
#call('python setup.py sdist bdist_wheel', shell=True, env=env)
call('twine upload dist/*', shell=True, env=env)

#cleanup
call('rm -rf test', shell=True, env=env)
call('rm -rf dist/*', shell=True, env=env)
call('rm -rf *egg-info', shell=True, env=env)
call('rm -rf src/*egg-info', shell=True, env=env)
call('rm -rf src/Shamsi/__pycache__', shell=True, env=env)

answer = input("Update GitLab?(y/n) ")
if answer == 'y':
    comment = input("Give Git Comment: ")
    call('git pull', shell=True)
    call('git add *', shell=True)
    call(f'git commit -am "{comment}"', shell=True)
    call('git push', shell=True)
